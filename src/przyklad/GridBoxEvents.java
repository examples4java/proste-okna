/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package przyklad;

import com.sun.javafx.scene.text.TextLine;
import com.sun.javafx.text.TextRun;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;

/**
 *
 * @author adminteb
 */
public class GridBoxEvents {
    public static GridPane createGrigPane() {
        GridPane gpane = new GridPane();
        gpane.setHgap(4);
        gpane.setVgap(3);
        
        Button btn = new Button();
        btn.setText("Say 'Hello World'");
        
        Label l = new Label();
        btn.onMouseClickedProperty().set(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (event.getButton() == MouseButton.PRIMARY)
                    l.setText("Wciśnięto lewy przycisk");
                if (event.getButton() == MouseButton.SECONDARY)
                    l.setText("Wciśnięto prawy przycisk");
            }
        });
        
        btn.onMouseMovedProperty().set(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                l.setText("Mysz znajduje się w pozycji: " + event.getSceneX() + ", " + event.getSceneY());
            }
        });
        
        TextField tf = new TextField();
        
        tf.onKeyTypedProperty().set(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                l.setText("Wciśnięto klawisz: " + event.getCharacter());
            }
        });
        
        btn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                
                System.out.println("Akcja: " + event.getClass().toGenericString() );
                System.out.println("Źródło: " + event.getSource().toString() );
            }
        });
        
        gpane.add(tf, 0,0);
        gpane.add(btn, 1, 0);
        gpane.add(l, 2, 1);
        
        return gpane;
    }
}
