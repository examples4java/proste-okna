/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package przyklad;

import static javafx.application.Application.STYLESHEET_MODENA;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

/**
 *
 * @author adminteb
 */
public class BorderBoxExample {
    public static BorderPane createBorderStack() {
        Button btn = new Button();
        btn.setText("Say 'Hello World'");
        btn.setOnAction(new EventHandler<ActionEvent>() {
            
            @Override
            public void handle(ActionEvent event) {
                System.out.println("Hello World!");
            }
        });
        
        Button btn2 = new Button();
        btn2.setText("Przycisk1'");
        Button btn3 = new Button();
        btn3.setText("Przycisk 2");
        Button btn4 = new Button();
        btn4.setText("Przycisk 3");
        Button btn5 = new Button();
        btn5.setText("Przycisk 4");
        Button btn6 = new Button();
        btn6.setPrefSize(200, 100);
        btn6.setText("Przycisk 5");
        Text txt = new Text("Przykład tekstu");
        txt.setFont(new Font(STYLESHEET_MODENA, 20));
        txt.setFill(Color.RED);
        Background ground = new Background(new BackgroundFill(Color.BLUE, new CornerRadii(2), new Insets(3)));
        StackPane sp = new StackPane();
        sp.setBackground(ground);
        sp.getChildren().addAll(txt,btn6);
        BorderPane bp = new BorderPane();
        
        bp.setCenter(btn);
        
        HBox hb = new HBox();
        hb.setOpacity(0.5);
        hb.setPadding(new Insets(10));
        hb.getChildren().add(btn2);
        hb.getChildren().add(btn3);
        hb.getChildren().addAll(btn4,btn5);
        //hb.getChildren().add(2,btn6);
        bp.setTop(hb);
        
        VBox vb = new VBox();
        vb.getChildren().add(sp);
        bp.setBottom(vb);
        return bp;
    }
}
