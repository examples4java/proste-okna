/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package przyklad;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;

/**
 *
 * @author adminteb
 */
public class GridBoxExample {
    public static GridPane createGrigPane() {
        GridPane gpane = new GridPane();
        gpane.setHgap(4);
        gpane.setVgap(3);
        
        Button btn = new Button();
        btn.setText("Say 'Hello World'");
        btn.setOnAction(new EventHandler<ActionEvent>() {
            
            @Override
            public void handle(ActionEvent event) {
                System.out.println("Hello World!");
            }
        });
        
        Button btn2 = new Button();
        btn2.setText("Przycisk1'");
        Button btn3 = new Button();
        btn3.setText("Przycisk 2");
        Button btn4 = new Button();
        btn4.setText("Przycisk 3");
        Button btn5 = new Button();
        btn5.setText("Przycisk 4");
        Button btn6 = new Button();
        btn6.setPrefSize(200, 100);
        btn6.setText("Przycisk 5");
        
        gpane.add(btn, 2, 3);
        gpane.addRow(0, btn6,btn5,btn2);
        gpane.addColumn(1, btn4,btn3);
        return gpane;
    }
}
